import React from "react";
// node.js library that concatenates classes (strings)
import classnames from "classnames";
import axios from 'axios';

import jwt_decode from "jwt-decode";
import setAuthToken from "../../utils/setAuthToken";
import { setCurrentUser, logoutUser } from "../../actions/authActions";
import store from "../../store";


// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Alert
} from "reactstrap";

// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  Media
} from "reactstrap";


import Header from "components/Headers/Header.js";
import Dictionnary from "layouts/Dictionnary";

// Check for token to keep user logged in
if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());

    // Redirect to login
    window.location.href = "./login";
  }
}

let prev = 0;
let next = 0;
let last = 0;
let first = 0;


class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeNav: 1,
      chartExample1Data: "data1",
      dictionnary: [],
      id: 0,
      word: '',
      synonyme: [],
      NewKeyword: '',
      NewSynonyme: '',
      ShowUpdate: false,
      ShowVisit: false,
      NewWord: '',
      showAlertAdd: false,
      showAlertDel: false,
      showAlertUpd: false,
      currentPage: 1,
      todosPerPage: 3,
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleLastClick = this.handleLastClick.bind(this);
    this.handleFirstClick = this.handleFirstClick.bind(this);

  }
  toggleNavs = (e, index) => {
    e.preventDefault();
    this.setState({
      activeNav: index,
      chartExample1Data:
        this.state.chartExample1Data === "data1" ? "data2" : "data1"
    });
  };

  //show list of keywords when component is ready 
  componentDidMount() {
    this.GetKeywords();

  }

  GetKeywords() {
    axios.get('/api/dictionnary')
      .then(res => {
        console.log('list dictionnary', res.data);
        const dictionnary = res.data;
        this.setState({ dictionnary, synonyme: [] });
      })
  }

  AddKeyword = () => {
    let found = false;
    this.state.dictionnary.forEach(element => {
      if (element.toString().toLowerCase().indexOf(this.state.NewKeyword.toString().toLowerCase()) != -1)
        found = true;
    });

    if (found == false && this.state.NewKeyword != '') {
      axios.post('/api/dictionnary/ajouterWord/' + this.state.NewKeyword.toString().toLowerCase())
        .then(res => {
          console.log(res);
          console.log(res.data);
          this.GetKeywords();

          this.setState({
            NewKeyword: '',
            showAlertAdd: true,
            ShowUpdate: false,
            ShowVisit: false,
          });

          setInterval(() => {
            this.setState({
              showAlertAdd: false,
            });
          }, 4000);
        })
        .catch(err => { });
    } else {
      this.setState({
        NewKeyword: ''
      });
    }
  }

  GoUpdateKeyword = (keyword) => {
    this.setState({
      id: keyword._id,
      word: keyword.word,
      synonyme: keyword.synonyme,
      ShowUpdate: true,
      ShowVisit: false,
      NewWord: keyword.word
    });
  }

  UpdateKeyword = () => {
    axios.put('/api/dictionnary/updateWord/' + this.state.word + '/' + this.state.NewWord)
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.GetKeywords();
        this.setState({
          ShowUpdate: false,
          ShowVisit: false,
          showAlertUpd: true,
          ShowUpdate: false,
          ShowVisit: false,
        });

        setInterval(() => {
          this.setState({
            showAlertUpd: false,
          });
        }, 4000);
      })
  }

  DeleteKeyword = (keyword) => {
    axios.delete('/api/dictionnary/supprimer/' + keyword)
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.GetKeywords();

        this.setState({
          showAlertDel: true,
          ShowUpdate: false,
          ShowVisit: false,
        });

        setInterval(() => {
          this.setState({
            showAlertDel: false,
          });
        }, 4000);
      })
  }

  VisitKeyword = (keyword) => {

    this.setState({
      id: keyword._id,
      word: keyword.word,
      synonyme: keyword.synonyme,
      ShowVisit: true,
      ShowUpdate: false
    });

  }

  AddSynonyme = () => {

    if (this.state.synonyme.indexOf(this.state.NewSynonyme.toString().toLowerCase()) == -1 && this.state.NewSynonyme != '') {
      axios.post('/api/dictionnary/AjouterSynonyme/' + this.state.word + '/' + this.state.NewSynonyme.toString().toLowerCase())
        .then(res => {
          var ListSynonyme = this.state.synonyme;
          ListSynonyme.push(this.state.NewSynonyme.toString().toLowerCase());

          this.setState({
            synonyme: ListSynonyme,
            NewSynonyme: '',
            showAlertAdd: true
          });

          setInterval(() => {
            this.setState({
              showAlertAdd: false
            });
          }, 4000);
        })
    }

  }

  DeleteSynonyme = (word) => {
    console.log(word);
    console.log(this.state.synonyme);
    var newSynonyme = this.state.synonyme;
    newSynonyme.splice(newSynonyme.indexOf(word), 1);
    console.log('!!!!' + newSynonyme);

    axios.delete('/api/dictionnary/deleteSynonyme/' + this.state.word + '/' + word)
      .then(res => {
        console.log(res);
        console.log(res.data);

        this.setState({
          synonyme: newSynonyme,
          showAlertDel: true
        });

        setInterval(() => {
          this.setState({
            showAlertDel: false
          });
        }, 4000);
      })

  }

  handleClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  handleLastClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: last
    });
  }
  handleFirstClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: 1
    });
  }

  render() {

    let { dictionnary, currentPage, todosPerPage } = this.state;

    // Logic for displaying current todos
    let indexOfLastTodo = currentPage * todosPerPage;
    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    let currentDictionnary = dictionnary.slice(indexOfFirstTodo, indexOfLastTodo);
    prev = currentPage > 0 ? (currentPage - 1) : 0;
    last = Math.ceil(dictionnary.length / todosPerPage);
    next = (last === currentPage) ? currentPage : currentPage + 1;

    // Logic for displaying page numbers
    let pageNumbers = [];
    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }

    return (
      <>
        <Header />
        {/* Page content */}
        <br></br>
        <Alert color="success" hidden={!this.state.showAlertAdd}>
          <strong>Success!</strong> Data was added successfully.
        </Alert>
        <Alert color="danger" hidden={!this.state.showAlertDel}>
          <strong>Success!</strong> Data was deleted successfully.
        </Alert>
        <Alert color="info" hidden={!this.state.showAlertUpd}>
          <strong>Success!</strong> Data was updated successfully.
        </Alert>
        <br></br> <br></br>
        <Container className="mt--7" fluid>
          <Row className="mt-5">
            <Col className="mb-5 mb-xl-0" xl="8">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">List of Keywords</h3>
                    </div>
                    <div className="col text-right">
                      <Form className="navbar-search  form-inline mr-3 d-none d-md-flex ml-lg-auto">
                        <Input placeholder="Add Keyword" type="text" value={this.state.NewKeyword} onChange={event => this.setState({ NewKeyword: event.target.value })} />
                        <Button
                          color="primary"
                          onClick={e => this.AddKeyword()}
                          size="sm">
                          Add
                        </Button>
                      </Form>
                    </div>
                  </Row>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Keyword name</th>
                      <th scope="col">Number of Synonyme</th>
                      <th scope="col">Actions</th>
                    </tr>
                  </thead>
                  <tbody>

                    {currentDictionnary.map(keyword =>
                      <tr key={keyword._id}>
                        <td scope="row">{keyword.word}</td>
                        <td scope="row">{keyword.synonyme.length}</td>
                        <td>
                          <Button
                            color="info"
                            onClick={e => this.GoUpdateKeyword(keyword)}
                            size="sm">
                            Update
                              </Button>

                          <Button
                            color="secondary"
                            onClick={e => this.VisitKeyword(keyword)}
                            size="sm">
                            Visit
                              </Button>

                          <Button
                            color="danger"
                            onClick={e => this.DeleteKeyword(keyword.word)}
                            size="sm">
                            Delete
                              </Button>
                        </td>
                      </tr>)
                    }
                  </tbody>
                </Table>
                <CardFooter className="py-4">
                  <ul id="page-numbers">
                    <nav>
                      <Pagination className="pagination justify-content-center"
                        listClassName="justify-content-center">
                        <PaginationItem>
                          {prev === 0 ? <PaginationLink disabled><i className="fa fa-angle-left" /></PaginationLink> :
                            <PaginationLink onClick={this.handleClick} id={prev} href={prev}> <i className="fa fa-angle-left" onClick={this.handleClick} id={prev} href={prev} /> </PaginationLink>
                          }
                        </PaginationItem>
                        {
                          pageNumbers.map((number, i) =>
                            <Pagination key={i}>
                              <PaginationItem active={pageNumbers[currentPage - 1] === (number) ? true : false} >
                                <PaginationLink onClick={this.handleClick} href={number} key={number} id={number}>
                                  {number}
                                </PaginationLink>
                              </PaginationItem>
                            </Pagination>
                          )}

                        <PaginationItem>
                          {
                            currentPage === last ? <PaginationLink disabled><i className="fa fa-angle-right" disabled /></PaginationLink> :
                              <PaginationLink onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}><i className="fa fa-angle-right" onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]} /></PaginationLink>
                          }
                        </PaginationItem>
                      </Pagination>
                    </nav>
                  </ul>
                </CardFooter>
              </Card>
            </Col>

            <Col xl="4" hidden={!this.state.ShowVisit}>
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">List of Synonyme {this.state.word}</h3>
                    </div>
                    <div className="col text-right">
                      <Form className="navbar-search  form-inline mr-3 d-none d-md-flex ml-lg-auto">
                        <Input placeholder="Add Synonyme" type="text" value={this.state.NewSynonyme} onChange={event => this.setState({ NewSynonyme: event.target.value })} />
                        <Button
                          color="primary"
                          onClick={e => this.AddSynonyme()}
                          size="sm">
                          Add
                          </Button>
                      </Form>
                    </div>
                  </Row>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Synonyme</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.synonyme.map(word =>
                      <tr key={this.state.synonyme.indexOf(word)}>
                        <td scope="row">{word}</td>
                        <td>
                          <Button
                            color="danger"
                            onClick={e => this.DeleteSynonyme(word)}
                            size="sm">
                            X
                              </Button>
                        </td>
                      </tr>)
                    }
                  </tbody>
                </Table>
              </Card>
            </Col>

            <Col xl="4" hidden={!this.state.ShowUpdate}>
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">Update Keyword : {this.state.word}</h3>
                    </div>
                    <div className="col text-right">
                      <Form className="navbar-search  form-inline mr-3 d-none d-md-flex ml-lg-auto">
                        <Input placeholder="keyWord Name" type="text" value={this.state.NewWord} onChange={event => this.setState({ NewWord: event.target.value })} />
                        <Button
                          color="info"
                          onClick={e => this.UpdateKeyword()}
                          size="sm">
                          Update
                          </Button>
                      </Form>
                    </div>

                  </Row>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Synonyme</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.synonyme.map(word =>
                      <tr key={this.state.synonyme.indexOf(word)}>
                        <td scope="row">{word}</td>
                        <td>
                          <Button
                            color="danger"
                            onClick={e => this.DeleteSynonyme(word)}
                            size="sm">
                            X
                              </Button>
                        </td>
                      </tr>)
                    }
                  </tbody>
                </Table>
              </Card>
            </Col>

          </Row>
        </Container>
      </>
    );
  }
}

export default List;
