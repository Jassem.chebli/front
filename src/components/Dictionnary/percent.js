import React, { useState } from 'react';
import { Line, Circle } from "rc-progress";

 const Percent=( {per}) =>{
    
    
    const colorMap = ["#3FC7FA", "#85D262", "#FE8C6A"];
    const value= Math.trunc(per*100)
    const [percent, setPercent] = useState(value);
    const [color, setColor] = useState(colorMap[parseInt(Math.random() * 3, 10)]);
    return (
        <div>
                <Circle
                  percent={percent}
                  strokeWidth="6"
                  strokeLinecap="round"
                  strokeColor={color}
                />
        </div>
    )
}

export default Percent
