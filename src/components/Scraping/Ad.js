import React from "react";
// node.js library that concatenates classes (strings)
import classnames from "classnames";
import axios from "axios";
import { Line, Circle } from "rc-progress";

import jwt_decode from "jwt-decode";
import setAuthToken from "../../utils/setAuthToken";
import { setCurrentUser, logoutUser } from "../../actions/authActions";
import store from "../../store";
import * as use from "@tensorflow-models/universal-sentence-encoder";
import { trainModel, suggest } from "../ML/Model";
import Percent from "../Dictionnary/percent";

import { FacebookProvider, LoginButton, MessageUs } from 'react-facebook';
import MessengerSendToMessenger from 'react-messenger-send-to-messenger';


// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  Media,
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Alert
} from "reactstrap";

import Header from "components/Headers/Header.js";
import Ad from "layouts/Ad";
import { ButtonGroup } from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// Check for token to keep user logged in
if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());

    // Redirect to login
    window.location.href = "./login";
  }
}


let prev = 0;
let next = 0;
let last = 0;
let first = 0;


class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeNav: 1,
      chartExample1Data: "data1",
      ad: [],
      adfiltred: [],
      model: null,
      encoder: null,
      color: ["#3FC7FA", "#85D262", "#FE8C6A"],
      loading: true,
      showAlertAdd: false,
      showAlertDel: false,
      showAlertUpd: false,
      currentPage: 1,
      todosPerPage: 6,
      cSelected: 0,
      scrapCounter: 0
    };


    this.handleClick = this.handleClick.bind(this);
    this.handleLastClick = this.handleLastClick.bind(this);
    this.handleFirstClick = this.handleFirstClick.bind(this);
  }



  loadModel = async () => {
    const sentenceEncoder = await use.load();
    this.setState({ encoder: sentenceEncoder });
    const trainedModel = await trainModel(sentenceEncoder);
    this.setState({ model: trainedModel });

  }
  componentDidMount() {
    this.loadModel().then(() => this.GetScrapedData());
  }

  toggleNavs = (e, index) => {
    e.preventDefault();
    this.setState({
      activeNav: index,
      chartExample1Data:
        this.state.chartExample1Data === "data1" ? "data2" : "data1",
    });
  };

  //show list of keywords when component is ready


  GetScrapedData = async () => {
    await axios.get("/api/ad/tunisieAnnonce/").then(async (res) => {
      const ad = res.data;
      console.log(ad)
      this.setState({ ad: ad, adfiltred: ad });



      //this.setState({loading:!this.state.loading, ad:ad});

    });
  };
  dataPrediction = async () => {
    this.setState({ loading: !this.state.loading })

    const add = [];
    let { ad } = this.state;
    await ad.forEach((elem) => {
      console.log(elem)

      this.handlePredict(elem).then((res) => {
        add.push({ ...elem, ["intent"]: Math.trunc(res * 100) })
        axios
          .put('/api/ad/tunisieAnnonce/update/' + elem._id + '/' + Math.trunc(res * 100))
          .then((res) => {
            console.log(res);
            console.log(res.data);
          })
          .catch((err) => {
            console.log(err);
          });
      }).then(() => {
        this.setState({ loading: !this.state.loading })
        this.setState({ ad: add });

      }
      );
    });


  }

  GetScrapedDataTayara = async () => {
    this.setState({ loading: !this.state.loading })
    await axios.get("/api/ad/tayara/").then(async (res) => {
      const ad = res.data;
      if (res.data.length == 0) {
        toast.warning('No new data to scrap from Tayara', {
          position: "top-right",
          autoClose: 4000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        this.setState({ loading: !this.state.loading })
      }
      else {
        ad.forEach(async (link) => {
          this.GetScrapedDataTayaraDetail(link, res.data.length);
        })
      }


    })

  };

  GetScrapedDataTayaraDetail = async (link, len) => {
    let linked = {
      link: link.toString()
    }
    console.log(linked)
    await axios.post("/api/ad/tayara/detail", { lin: link }).then(async (res) => {
      this.setState({ scrapCounter: this.state.scrapCounter + 1 })
      const item = res.data;
      this.setState({ ad: [...this.state.ad, item], adfiltred: [...this.state.adfiltred, item] });
      console.log(this.state.scrapCounter)
      if (this.state.scrapCounter == len) {
        this.setState({ loading: !this.state.loading })
        toast.success('Scraping from Tayara is done', {
          position: "top-right",
          autoClose: 4000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        this.setState({ scrapCounter: 0 })
      }
    });
  };


  ScrapingData = () => {
    axios
      .get("/api/ad/tunisieAnnonce/scraping/mobile/5")
      .then((res) => {
        console.log(res);
        console.log(res.data);
        this.GetScrapedData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  UpdateTreated = (Ad) => {
    axios.put('/api/ad/tunisieAnnonce/update/isTreated/' + Ad._id + '/' + !Ad.isTreated)
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.GetScrapedData();

        this.setState({
          showAlertUpd: true,
        });

        setInterval(() => {
          this.setState({
            showAlertUpd: false,
          });
        }, 4000);
      }).then(axios.post("/api/model/ajouterRowBuyer/" + Ad.content));
  };

  DeleteScrapingData = (Ad) => {
    axios.delete('/api/ad/tunisieAnnonce/delete/' + Ad._id)
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.GetScrapedData();

        this.setState({
          showAlertDel: true,
        });

        setInterval(() => {
          this.setState({
            showAlertDel: false,
          });
        }, 4000);
      }).then(axios.post("/api/model/ajouterRowNotBuyer/" + Ad.content));
  };



  handlePredict = async (text) => {
    return await suggest(this.state.model, this.state.encoder, text, 0.64);
  };

  DeleteAll() {
    axios.delete('/api/ad/tunisieAnnonce/delete/')
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.GetScrapedData();

        this.setState({
          showAlertDel: true,
        });

        setInterval(() => {
          this.setState({
            showAlertDel: false,
          });
        }, 4000);
      })
  }

  VisitAd = (Ad) => {
    window.open(Ad.link);
  }

  showImage = (img) => {
    window.open(img);
  }

  ContactByEmail = (data) => {
    data.email = 'rachid.arafa@esprit.tn'
    axios.get('/api/contact/email/' + data.email)
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  ContactBymessage = (data) => {
    axios.post('/api/contact/sms/' + data.mobile)
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  handleClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  handleLastClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: last
    });
  }
  handleFirstClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: 1
    });
  }

  handleResponse = (data) => {
    console.log(data);
  }

  handleError = (error) => {
    this.setState({ error });
    console.log(error);
  }
  fil = () => {
    const x = []
    console.log(this.state.cSelected);


    if (this.state.cSelected == 1) {
      this.setState({
        adfiltred: this.state.ad.filter(e =>
          e.intentnum < 30
        )
      })
    }
    if (this.state.cSelected == 2)

      this.setState({
        adfiltred: this.state.ad.filter(e =>
          e.intentnum > 30 && e.intentnum < 60
        )
      })
    if (this.state.cSelected == 3) {


      this.setState({
        adfiltred: this.state.ad.filter(e =>
          e.intentnum > 60
        )
      })
      console.log(this.state.adfiltred)

    }





  }
  onCheckboxBtnClick = (selected) => {

    this.setState({ cSelected: selected });


    this.fil();


  }
  sortad = () => {
    let x = this.state.adfiltred.sort(function (a, b) { return b.intentnum - a.intentnum });
    this.setState({ adfiltred: x });
  }
  render() {

    let { ad, currentPage, todosPerPage } = this.state;

    // Logic for displaying current todos
    let indexOfLastTodo = currentPage * todosPerPage;
    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    let adf = this.state.adfiltred;
    let currentAd = adf.slice(indexOfFirstTodo, indexOfLastTodo);
    prev = currentPage > 0 ? (currentPage - 1) : 0;
    last = Math.ceil(ad.length / todosPerPage);
    next = (last === currentPage) ? currentPage : currentPage + 1;

    // Logic for displaying page numbers
    let pageNumbers = [];
    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }

    return (
      <>
        <Header />
        <ToastContainer />
        {/* Page content */}
        <br></br>
        <Alert color="success" hidden={!this.state.showAlertAdd}>
          <strong>Success!</strong> Data was added successfully.
        </Alert>
        <Alert color="danger" hidden={!this.state.showAlertDel}>
          <strong>Success!</strong> Data was deleted successfully.
        </Alert>
        <Alert color="info" hidden={!this.state.showAlertUpd}>
          <strong>Success!</strong> Data was updated successfully.
        </Alert>
        <br></br> <br></br>
        <Container className="mt--7" fluid>
          <Row className="mt-5">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">List of Scraping Ad :</h3>

                    </div>
                    <div className="col text-center" >
                      <Form className="navbar-search  form-inline mr-3 d-none d-md-flex ml-lg-auto">
                        <Button
                          color="primary"
                          onClick={(e) => this.ScrapingData()}
                          size="sm"
                        >
                          Scraping
                        </Button>
                        <Button
                          color="warning"
                          onClick={(e) => this.GetScrapedDataTayara()}
                          size="sm"
                        >
                          Scraping Tayara
                        </Button>
                        <Button
                          color="danger"
                          onClick={e => this.DeleteAll()}
                          size="sm">
                          Delete All
                        </Button>
                        <Button
                          color="primary"
                          size="sm"
                          onClick={e => this.dataPrediction()}
                        >
                          Predict
                        </Button>

                        <ButtonGroup style={{ marginTop: '20px' }}>
                          <Button color="primary" onClick={() => this.onCheckboxBtnClick(1)} active={this.state.cSelected == 1}>0%-30%</Button>
                          <Button color="primary" onClick={() => this.onCheckboxBtnClick(2)} active={this.state.cSelected == 2}>30%-60%</Button>
                          <Button color="primary" onClick={() => this.onCheckboxBtnClick(3)} active={this.state.cSelected == 3}>60%-100%</Button>
                          <Button color="primary" onClick={() => { this.setState({ adfiltred: ad, cSelected: 0 }) }} >clear</Button>
                          <Button
                            color="info"
                            onClick={e => this.sortad()}
                          >
                            Sort
                          </Button>

                        </ButtonGroup>
                      </Form>

                    </div>
                    <div hidden={this.state.loading} className="col " style={{ float: "right" }}>

                      <div className="clearfix">
                        <div className="spinner-border float-right" role="status">
                          <span className="sr-only">Loading...</span>
                        </div>
                      </div>
                    </div>
                  </Row>
                </CardHeader>
                <div id="lossContainer" />
                <div id="accContainer" />

                <Table responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Description</th>
                      <th scope="col">Category</th>
                      <th scope="col">Price</th>
                      <th scope="col">Adresse</th>
                      <th scope="col">intent</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    {currentAd.map(data => {
                      if (data.isTreated) {
                        return (<tr key={data._id} style={{ backgroundColor: '#b3d9ff' }}>
                          <td scope="col"> <textarea style={{ borderColor: '#b3d9ff', backgroundColor: '#b3d9ff', color: '#525f7f' }} value={data.content} contentEditable='false' cols='35' suppressContentEditableWarning={true} rows={(data.content.length + 36) / 36}></textarea></td>
                          <td scope="col"> {data.category}</td>
                          <td scope="col"> {data.price.replace('Dinar Tunisien', '')}</td>
                          <td scope="col"> <textarea style={{ borderColor: '#b3d9ff', backgroundColor: '#b3d9ff', color: '#525f7f' }} value={data.adresse} contentEditable='false' cols='20' suppressContentEditableWarning={true} rows={(data.adresse.length + 40) / 27}></textarea></td>

                          <td >

                            <Line
                              percent={data.intentnum}
                              strokeWidth="6"
                              strokeLinecap="round"
                              strokeColor={data.intentnum > 40 ? "#85D262" : data.intentnum < 40 ? "#FE8C6A" : data.intentnum > 60 ? "#3FC7FA" : "#3FC7FA"}
                            />

                            {data.intentnum} %
                        </td>
                          <td style={{ maxWidth: '10px', minWidth: '10px', width: '10px' }}>
                            <UncontrolledDropdown>
                              <DropdownToggle
                                className="btn-icon-only text-light"
                                href="#pablo"
                                role="button"
                                size="sm"
                                color="black"
                                onClick={e => e.preventDefault()}
                              >
                                <i className="fas fa-ellipsis-v" style={{ color: '#999999' }} />
                              </DropdownToggle>
                              <DropdownMenu className="dropdown-menu-arrow" right>
                                <DropdownItem
                                  onClick={e => this.UpdateTreated(data)}
                                >
                                  Switch Treated
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.VisitAd(data)}
                                >
                                  Visit
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.ContactByEmail(data)}
                                >
                                  Contact By Email
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.ContactBymessage(data)}
                                >
                                  Contact By message
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.DeleteScrapingData(data)}
                                >
                                  Delete
                            </DropdownItem>
                              </DropdownMenu>
                            </UncontrolledDropdown>
                          </td>
                        </tr>)
                      } else {
                        return (<tr key={data._id}>
                          <td scope="col" style={{ width: "50px", height: "50px" }}> <textarea style={{ borderColor: 'white', color: '#525f7f' }} value={data.content} contentEditable='false' cols='35' suppressContentEditableWarning={true} rows={(data.content.length + 36) / 36}></textarea></td>
                          <td scope="col"> {data.category}</td>
                          <td scope="col"> {data.price.replace('Dinar Tunisien', '')}</td>
                          <td scope="col"> <textarea style={{ borderColor: 'white', backgroundColor: 'white', color: '#525f7f' }} value={data.adresse} contentEditable='false' cols='20' suppressContentEditableWarning={true} rows={(data.adresse.length + 40) / 27}></textarea></td>

                          <td >

                            <Line
                              percent={data.intentnum}
                              strokeWidth="6"
                              strokeLinecap="round"
                              strokeColor={data.intentnum > 50 ? "#85D262" : data.intentnum < 50 ? "#FE8C6A" : data.intentnum > 60 ? "#3FC7FA" : "#3FC7FA"}
                            />

                            {data.intentnum} %
                        </td>

                          <td>
                            <UncontrolledDropdown>
                              <DropdownToggle
                                className="btn-icon-only text-light"
                                href="#pablo"
                                role="button"
                                size="sm"
                                color=""
                                onClick={e => e.preventDefault()}
                              >
                                <i className="fas fa-ellipsis-v" />
                              </DropdownToggle>
                              <DropdownMenu className="dropdown-menu-arrow" right>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.UpdateTreated(data)}
                                >
                                  Switch Treated
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.VisitAd(data)}
                                >
                                  Visit
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.ContactByEmail(data)}
                                >
                                  Contact By Email
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.ContactBymessage(data)}
                                >
                                  Contact By message
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.DeleteScrapingData(data)}
                                >
                                  Delete
                            </DropdownItem>
                              </DropdownMenu>
                            </UncontrolledDropdown>
                          </td>
                        </tr>)
                      }
                    }


                    )
                    }
                  </tbody>
                </Table>
                <CardFooter className="py-4">
                  <ul id="page-numbers">
                    <nav>
                      <Pagination className="pagination justify-content-center"
                        listClassName="justify-content-center">
                        <PaginationItem>
                          {prev === 0 ? <PaginationLink disabled><i className="fa fa-angle-left" /></PaginationLink> :
                            <PaginationLink onClick={this.handleClick} id={prev} href={prev}> <i className="fa fa-angle-left" onClick={this.handleClick} id={prev} href={prev} /> </PaginationLink>
                          }
                        </PaginationItem>
                        {
                          pageNumbers.map((number, i) =>
                            <Pagination key={i}>
                              <PaginationItem active={pageNumbers[currentPage - 1] === (number) ? true : false} >
                                <PaginationLink onClick={this.handleClick} href={number} key={number} id={number}>
                                  {number}
                                </PaginationLink>
                              </PaginationItem>
                            </Pagination>
                          )}

                        <PaginationItem>
                          {
                            currentPage === last ? <PaginationLink disabled><i className="fa fa-angle-right" disabled /></PaginationLink> :
                              <PaginationLink onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}><i className="fa fa-angle-right" onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]} /></PaginationLink>
                          }
                        </PaginationItem>
                      </Pagination>
                    </nav>
                  </ul>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default List;
