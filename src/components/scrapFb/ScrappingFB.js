import React, {
  useState,
  useEffect
} from "react";
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from "react-bootstrap-table2-paginator";
import * as ReactBootSrap from "react-bootstrap";
import Header from "components/Headers/Header.js";
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
const App = () => {
  const [player, setPlayer] = useState([]);
  const [loading, setLoading] = useState(false);
  const getPlayerData = async () => {
    try {
      const res = await axios.get('https://pidevtrustit.herokuapp.com/api/ad/facebook/getData/');
      console.log(res.data);
      setPlayer(res.data);
      setLoading(true);
    } catch (e) {
      console.log(e);
    }
  }
  const columns = [{
    dataField: "content",
    text: "content",
    filter: textFilter()
  }];

  useEffect(() => {
    getPlayerData()
  }, [])

  return <div className = "App" >
    < Header / > {
      loading ? ( <
        BootstrapTable keyField = "name"
        data = {
          player
        }
        columns = {
          columns
        }

        pagination = {
          paginationFactory()
        }
        filter={ filterFactory()}
        />

      ) : ( <
        ReactBootSrap.Spinner animation = "border" / >
      )
    }


    <
    /div>
}
export default App;