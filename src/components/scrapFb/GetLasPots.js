import React , {useState , useEffect}  from "react";
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from "react-bootstrap-table2-paginator";
import * as ReactBootSrap from "react-bootstrap";
import Header from "components/Headers/Header.js";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Alert
} from "reactstrap";

// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  Media
} from "reactstrap";

const Apps = ()=>{
  const [player , setPlayer]= useState([]);
  const [state, setState]= useState([]);
  const [loading , setLoading]= useState(false);
  const getPlayerData = async()=>{
    console.log(state.urlWebsite)
    try{
      const res = await axios.get('/api/ad/facebook/getLastPosts?url='+state.urlWebsite);
      console.log(res.data) ;
      setPlayer(res.data);
      setLoading(true);
    } catch(e) {
        console.log(e);
      }
  }
  const columns = [
    {dataField : "content",text : "content"}
  ];

 /* useEffect(()=>{
    getPlayerData()
  },[])
*/
  return <div className="Apps">
     <Header />

     <br></br> <br></br>
        <Container className="mt--7" fluid>
          <Row className="mt-5">
            <Col className="mb-5 mb-xl-0" xl="8">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">List of Last Posts Scrapping</h3>
                    </div>
                    <div className="col text-right">
                      <Form className="navbar-search  form-inline mr-3 d-none d-md-flex ml-lg-auto">
                        <Input placeholder="Add Keyword" type="text" onChange={event => setState({ urlWebsite: event.target.value })} />
                        <Button
                          color="primary"
                          onClick={e => getPlayerData()}
                          size="sm">
                          Add
                        </Button>
                      </Form>
                    </div>
                  </Row>
                </CardHeader>
                {loading ? (
                      <BootstrapTable 
                      keyField="name"
                      data = {player}
                      columns={columns}
                      pagination={paginationFactory()}
                      />
                ):(
                  <ReactBootSrap.Spinner animation="border"/>
                )}
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Keyword name</th>
                      <th scope="col">Number of Synonyme</th>
                      <th scope="col">Actions</th>
                    </tr>
                  </thead>
                  <tbody>    
                  </tbody>
                </Table>
         
              </Card>
            </Col>

          </Row>
        </Container>
  </div>
}
export default Apps ; 