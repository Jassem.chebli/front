import React, { useState, useEffect } from "react";
import { suggest, training } from "./Model";
import { Collapse, Button, CardBody, Card } from "reactstrap";
import { Line, Circle } from "rc-progress";

import _ from "lodash";
const NewPost = (model, encoder) => {
  const CONFIDENCE_THRESHOLD = 0.65;
  const [post, SetPost] = useState({
    content: "",
    intent: ""
  });
  const [isOpen, setIsOpen] = useState(false);
  const [prop, setProb] = useState(0);

  const [suggestedIcon, setSuggestedIcon] = useState(null);
  const handleNameChange = async event => {
    console.log(event.target.value);
    const postcontent = event.target.value;
    SetPost({
      ...post,
      content: postcontent
    });
    /* setTypeTimeout(
    setTimeout(async () => {
        const predictedIcon = await suggest(
            model,
            encoder,
            postcontent,
            CONFIDENCE_THRESHOLD
          );
          setSuggestedIcon(predictedIcon);
          console.log(`hedhy ${predictedIcon}`);
    }, 400)
  ); */
  };

  const toggle = async () => {
    setIsOpen(!isOpen);
    setTraintext("Training...");
    const retrainedModel = await training();
    model = retrainedModel;
    setIsOpen(!isOpen);
    setTraintext("The Model is trained");
  };

  const handlePredict = async () => {
   await suggest(
      model.model,
      encoder,
      post,
      CONFIDENCE_THRESHOLD
    ).then(res =>{ const colorMap = ["#3FC7FA", "#85D262", "#FE8C6A"];
    const value= Math.trunc(res*100)
    setPercent(value);
    console.log(percent);
    setColor(colorMap[parseInt(Math.random() * 3, 10)]);})
   
  };
  const circleContainerStyle = {
    width: "250px",
    height: "250px",
    display: "inline-block"
  };
  const constcolor = "#3FC7FA";
  const containerStyle = {
    width: "250px"
  };
  const [percent, setPercent] = useState(0);
  const [color, setColor] = useState("#3FC7FA");

  const changeState = () => {
    const colorMap = ["#3FC7FA", "#85D262", "#FE8C6A"];
    setPercent(suggestedIcon);
    console.log(percent);
    setColor(colorMap[parseInt(Math.random() * 3, 10)]);
  };
  const [traintext, setTraintext] = useState("Train The Model");

  return (
    <React.Fragment>
      
        <Button
          color="primary"
          onClick={toggle}
          style={{ marginBottom: "1rem" }}
          disabled={isOpen}
        >
          {traintext}
        </Button>
      
        <div class="input-group mb-3">
          
        <input
          type="text"
          className="form-control"
          placeholder="Recipient's username"
          aria-label="Recipient's username"
          aria-describedby="button-addon2"
          
          disabled={isOpen}
          label="Name"
          placeholder="your sentence"
          onChange={handleNameChange}
          value={post.content}
        />
  <div class="input-group-append">
    <button className="btn btn-outline-primary" type="button" id="button-addon2"           onClick={handlePredict}
>Predict</button>
  </div>
</div>

        <div id="lossContainer" />
        <div id="accContainer" />


    

        <div className="container">
          <div class="row">
            <div class="col-sm">
              <div style={circleContainerStyle} >
                <Circle
                  percent={percent}
                  strokeWidth="6"
                  strokeLinecap="round"
                  strokeColor={color}
                />
              </div>
            </div>
            <div class="col-sm">
              <div hidden={percent < 1}>
                <h4> {percent}% probability that the owner of this tweet would buy your service</h4>
              </div>{" "}
            </div>
          </div>
        </div>
    </React.Fragment>
  );
};
export default NewPost;
