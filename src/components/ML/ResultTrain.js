import React, { useState, useEffect } from "react";
import { render } from "react-dom";

import * as use from "@tensorflow-models/universal-sentence-encoder";
import { trainModel } from "./Model";

import { Card, Row } from "reactstrap";
import { Container } from "reactstrap";
import Header from "components/Headers/Header.js";

import NewPost from "./NewPost";

const ResultTrain = () => {
  const [tasks, setTasks] = useState({});
  const [model, setModel] = useState(null);
  const [encoder, setEncoder] = useState(null);

  useEffect(() => {
    const loadModel = async () => {
      const sentenceEncoder = await use.load();
      const trainedModel = await trainModel(sentenceEncoder);
      setEncoder(sentenceEncoder);
      console.log(sentenceEncoder);
      setModel(trainedModel);
      console.log(trainedModel);
    };
    loadModel();
  }, []);

  return (
    <>
    <Header/>
    <Container className="mt--7" fluid>
    
        <Card style={{ margin: "50px", padding: "20px" }}>
          <h3>Predict </h3>

          <div className="container " style={{ paddingTop: "50px" }}>
            <NewPost model={model} encoder={encoder}></NewPost>
          </div>
        </Card>
    </Container>
    </>
  );
};

export default ResultTrain;
