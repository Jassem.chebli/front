import React, { useState, useEffect } from "react";
import { Line, Circle } from "rc-progress";
export default class probResult extends React.Component {
  constructor(props) {
      super(props)

      this.state={
        percent: this.props.percent,
        color : "#3FC7FA"

      }
  }
  render() {
    const circleContainerStyle = {
        width: "250px",
        height: "250px",
        display: "inline-block"
      };
      const constcolor = "#3FC7FA";
      const containerStyle = {
        width: "250px"
      };
 
    
      const changeState = () => {
        const colorMap = ["#3FC7FA", "#85D262", "#FE8C6A"];
        const value = parseInt(Math.random() * 100, 10);
        this.setState(state => ({ percent: value , color : colorMap[parseInt(Math.random() * 3, 10)]}));
      };
    return (
    <React.Fragment>
            <h3>Line Progress {this.state.percent}%</h3>
    /* <div style={containerStyle}>
      <Line percent={this.state.percent} strokeWidth="4" strokeColor={this.state.color} />
      <Line
        percent={[this.state.percent / 2, this.state.percent / 2]}
        strokeWidth="4"
        strokeColor={[this.state.color, '#CCC']}
      />
    </div> */



    <h3>Circle Progress {this.state.percent}%</h3>
    <div style={circleContainerStyle}>
      <Circle percent={this.state.percent} strokeWidth="6" strokeLinecap="round" strokeColor={this.state.color} />
    </div>
    <p>
    <button type="button" onClick={changeState}>
        Change State
      </button>
    </p>
    </React.Fragment>


    );
  }
}
