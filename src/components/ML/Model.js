import * as tf from "@tensorflow/tfjs";
import * as tfvis from "@tensorflow/tfjs-vis";
import * as use from "@tensorflow-models/universal-sentence-encoder";
import axios from "axios";
var cleaner = require('node-textcleaner');
const data = require("../data/data.json");

/* const trainTasks = learnTodos.concat(exerciseTodos); */

const MODEL_NAME = "suggestion-model";
const N_CLASSES = 2;

const encodeData = async (encoder, sentence) => {
  console.log("1");
  var regExpr = /[^a-zA-Z0-9-. ]/g;
  const sentences = sentence.map(t => cleaner(t.text).replace(regExpr, ""));
  console.log("2");
  console.log(sentences);

  const embeddings = await encoder.embed(sentences);

  console.log(embeddings);

  console.log("3");

  return embeddings;
};








const training = async ()  => {
  const encoder = await use.load();
  let sdata= null;

  sdata=await axios.get("/api/model/").then((res)=>{return res.data; console.log("0");} )
  console.log("1");
  console.log(sdata)

  const xTrain = await encodeData(encoder, sdata);
  console.log("2");
  console.log(xTrain)
  const yTrain = tf.tensor2d(
    sdata.map(t => [t.intent === "buy" ? 1 : 0, t.intent === "no" ? 1 : 0])
  );
  console.log("3");
  console.log(yTrain)
  const model = tf.sequential();
  console.log("4");
  model.add(
    tf.layers.dense({
      inputShape: [xTrain.shape[1]],
      activation: "softmax",
      units: N_CLASSES
    })
  );
  console.log("5");
  model.compile({
    loss: "categoricalCrossentropy",
    optimizer: tf.train.adam(0.001),
    metrics: ["accuracy"]
  });
  console.log("6");
  const lossContainer = document.getElementById("loss-cont");
  const accContainer = document.getElementById("accContainer");

  console.log("7");
  const trainLogs = [];
  const beginMs = performance.now();

  await model.fit(xTrain, yTrain, {
    batchSize: 32,
    validationSplit: 0.1,
    shuffle: true,
    epochs: 500,
    callbacks: {
      onEpochEnd: (epoch, logs) => {
        // Plot the loss and accuracy values at the end of every training epoch.

        trainLogs.push(logs);
        tfvis.show.history(accContainer, trainLogs, ["loss", "acc"]);
      }
    }
  });
  console.log("8");

  await model.save(`localstorage://${MODEL_NAME}`);
  console.log("9");

  return model;
};





const trainModel = async encoder => {
  try {
    const loadedModel = await tf.loadLayersModel(
      `localstorage://${MODEL_NAME}`
    );
    console.log("Using existing model");
    return loadedModel;
  } catch (e) {
    console.log("Training new model");
  }

  return training();
};

const suggest = async (model, encoder, texttest, threshold) => {
  /*   if (!text.trim().includes(" ")) {
    return null;
  } */ const sentenceEncoder = await use.load();
   const trainedModel = await trainModel(sentenceEncoder);  
  console.log(model)
  const xPredict = await encodeData(sentenceEncoder, [{ text: texttest.content }]);

  const loadedModel = await tf.loadLayersModel(`localstorage://${MODEL_NAME}`);

  const prediction = await model.predict(xPredict).data();
  console.log(prediction[0]);
  console.log(prediction[1]);

return (prediction[0]);
};

export { suggest, trainModel , training};
