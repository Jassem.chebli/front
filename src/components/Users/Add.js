/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

import axios from 'axios';

// reactstrap components
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Container,
    Row,
    Col
} from "reactstrap";

import PropTypes from "prop-types";
import classnames from "classnames";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { addUser } from "../../actions/userActions";

class Add extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            password: "",
            password2: "",
            users: [],
            errors: {}
        };
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
            this.setState({
                errors: nextProps.errors
            });
        }
    }
    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };
    onSubmit = e => {
        e.preventDefault();
        const newUser = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            password2: this.state.password2
        };

        console.log(newUser);
        this.props.addUser(newUser, this.props.history);
        setTimeout(() => {
            this.props.closeAd()
        }, 500);
        

    };
    render() {
        const { errors } = this.state;
        return (
            <>
                {/* Page content */}
                <Container className="mt-7" fluid>
                    <Row>
                        <Col className="order-xl-1" xl="12">
                            <Card className="bg-secondary shadow">
                                <CardHeader className="bg-white border-0">
                                    <Row className="align-items-center">
                                        <Col xs="8">
                                            <h3 className="mb-0">Add user
                                            </h3>

                                        </Col>
                                        <Col className="text-right" xs="4">
                                            <Button
                                                color="danger"
                                                onClick = {this.props.closeAd}
                                                size="sm"
                                                className="modal-close"
                                            >
                                                <i className=" ni ni-fat-remove" />
                                            </Button>
                                        </Col>
                                    </Row>
                                </CardHeader>
                                <CardBody>
                                    <Form role="form" noValidate onSubmit={this.onSubmit}>
                                        <h6 className="heading-small text-muted mb-4">
                                            User information
                                        </h6>
                                        <div className="pl-lg-4">
                                            <Row>
                                                <Col lg="6">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-username"
                                                        >
                                                            Name
                                                        </label>
                                                        <Input onChange={this.onChange}
                                                            value={this.state.name}
                                                            error={errors.name}
                                                            id="name"
                                                            type="text"
                                                            className={classnames("", {
                                                                invalid: errors.name
                                                            })} />
                                                        <span className="red-text">{errors.name}</span>
                                                    </FormGroup>
                                                </Col>
                                                <Col lg="6">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-email"
                                                        >
                                                            Email address
                            </label>
                                                        <Input
                                                            onChange={this.onChange}
                                                            value={this.state.email}
                                                            error={errors.email}
                                                            type="email"
                                                            className={classnames("", {
                                                                invalid: errors.email
                                                            })}
                                                            id="email"
                                                        />
                                                        <span className="red-text">{errors.email}</span>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col lg="6">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-first-name"
                                                        >
                                                            Password
                            </label>
                                                        <Input
                                                            id="password"

                                                            onChange={this.onChange}
                                                            value={this.state.password}
                                                            error={errors.password}
                                                            type="password"
                                                            className={classnames("", {
                                                                invalid: errors.password
                                                            })} />
                                                        <span className="red-text">{errors.password}</span>
                                                    </FormGroup>
                                                </Col>
                                                <Col lg="6">
                                                    <FormGroup>
                                                        <label
                                                            className="form-control-label"
                                                            htmlFor="input-last-name"
                                                        >
                                                            Password confirmation
                            </label>
                                                        <Input
                                                            className="form-control-alternative"
                                                            type="text"
                                                            onChange={this.onChange}
                                                            value={this.state.password2}
                                                            error={errors.password2}
                                                            id="password2"
                                                            type="password"
                                                            className={classnames("", {
                                                                invalid: errors.password2
                                                            })} />
                                                        <span className="red-text">{errors.password2}</span>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <div className="text-center">
                                                <Button
                                                    type="submit"
                                                    className="mt-4" color="primary">
                                                    Create user
                  </Button>
                                            </div>
                                        </div>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}
Add.propTypes = {
    addUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps,
    { addUser }
)(withRouter(Add));