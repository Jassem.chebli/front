/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";
import { Provider } from "react-redux";
import store from "./store";
import "assets/plugins/nucleo/css/nucleo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "assets/scss/argon-dashboard-react.scss";
import "assets/css/custom.css"
import AdminLayout from "layouts/Admin.js";
import DictionnaryLayout from "layouts/Dictionnary.js";
import AdLayout from "layouts/Ad.js";
import Login from "components/Auth/Login.js";
import Register from "components/Auth/Register.js"
import PrivateRoute from "./components/private-route/PrivateRoute";
import ResultTrain from "components/ML/ResultTrain";
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
      <Route exact path="/" component={Login} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />

        {/* <Route path="/admin" render={props => <AdminLayout {...props} />} /> */}
        <PrivateRoute exact path="/admin/index" component={AdminLayout} />
        <PrivateRoute exact path="/admin/dictionnary" component={AdminLayout} />
        <PrivateRoute exact path="/admin/ad" component={AdminLayout} />
        <PrivateRoute exact path="/admin/ScrappingFB" component={AdminLayout} />
        <PrivateRoute exact path="/admin/predict" component={AdminLayout} />
        <PrivateRoute exact path="/admin/GetLasPots" component={AdminLayout} />
        <PrivateRoute exact path="/admin/users" component={AdminLayout} />
        <PrivateRoute exact path="/admin/users/add" component={AdminLayout} />
        <PrivateRoute exact path="/admin/user-profile" component={AdminLayout} />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
