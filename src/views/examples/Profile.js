/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import * as faceapi from 'face-api.js';
import axios from 'axios';
import jwt_decode from "jwt-decode";
import setAuthToken from "../../utils/setAuthToken";
import { setCurrentUser, logoutUser } from "../../actions/authActions";
import store from "../../store";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col
} from "reactstrap";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// core components
import UserHeader from "components/Headers/UserHeader.js";
import { any } from "@tensorflow/tfjs";


if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());

    // Redirect to login
    window.location.href = "./login";
  }
}

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      password2: "",
      errors: {},
      showcam: false,
      facedata: false,
      faceReady: false,
      camReady: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.videoTag = React.createRef();
    this.played = this.played.bind(this)
  }

  async openCam() {
    this.setState({ showcam: true });
    Promise.all([
      faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
      faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
      faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
      await faceapi.nets.ssdMobilenetv1.loadFromUri('/models')
    ]).then(this.startVideo())

  }
  closeCam() {
    const tracks = this.videoTag.current.srcObject.getTracks();
    tracks[0].stop();
    this.setState({ showcam: false });
    this.setState({
      camReady: false
    })
  }

  startVideo() {
    navigator.mediaDevices
      .getUserMedia({ video: true })
      .then(stream => this.videoTag.current.srcObject = stream)
      .catch(console.log);
  }
  async played() {

    const vid = document.getElementById('video')
    vid.width = 640
    vid.height = 480
    const ro = document.getElementById('ro')
    const canvas = faceapi.createCanvasFromMedia(vid)

    ro.appendChild(canvas)
    const displaySize = { width: vid.width, height: vid.height }
    faceapi.matchDimensions(canvas, displaySize)
    const entred = false;
    this.setState({
      camReady: true
    })
    let myInterval = setInterval(async () => {
      const detections = await faceapi.detectSingleFace(vid, new faceapi.TinyFaceDetectorOptions({ inputSize: 128 })).withFaceLandmarks().withFaceDescriptor()

      if (detections) {

        const resizedDetections = faceapi.resizeResults(detections, displaySize)
        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        faceapi.draw.drawDetections(canvas, resizedDetections)
        faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
        let descriptor = {
          label: jwt_decode(localStorage.getItem("jwtToken")).name,
          descriptors: detections.descriptor.toString()
        }
        if (entred == false) {
          this.AddDescription(descriptor);
          entred = true;
        }
        clearInterval(myInterval);

      }

    }, 50)
  }
  loadLabeledDesc = () => {
    const component = this;

    return new Promise(function (resolve, reject) {
      axios.get('/api/users/descriptors')
        .then((res) => {
          const descriptors = res.data;
          return Promise.all(
            descriptors.map(async (desc) => {
              if (jwt_decode(localStorage.getItem("jwtToken")).name === desc.label) {
                component.setState({
                  facedata: true
                })
              }
            })
          )
        }).catch(err => { console.log(err) });
      component.setState({
        faceReady: true
      })
      resolve(true)
    });

  }

  UpdateProfile = () => {
    let user;
    if (this.state.password === "") {
      user = {
        name: this.state.name,
        email: this.state.email,
      }
    }
    else {
      user = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        password2: this.state.password2
      }
    }

    axios.put('/api/users/' + jwt_decode(localStorage.getItem("jwtToken")).id, user)
      .then(res => {
        toast.success('Updated successfully', {
          position: "top-right",
          autoClose: 4000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })
  }


  AddDescription = async (descriptors) => {

    if (this.state.facedata) {

      axios.put('/api/users/descriptors', descriptors)
        .then(res => {

          toast.success('Face recognition data updated successfully ', {
            position: "top-right",
            autoClose: 4000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          this.closeCam();
          this.loadLabeledDesc();

        })
    }
    else {
      axios.post('/api/users/descriptors', descriptors)
        .then(res => {

          toast.success('Face recognition data added successfully ', {
            position: "top-right",
            autoClose: 4000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          this.closeCam();
          this.loadLabeledDesc();
        })
    }
  }

  GetUser = () => {
    axios.get('/api/users/' + jwt_decode(localStorage.getItem("jwtToken")).id)
      .then(res => {
        const user = res.data;
        console.log(res.data)
        this.setState({
          name: user.name,
          email: user.email,
        });

      }).catch(err => { console.log(err) });
  }
  componentDidMount() {
    this.loadLabeledDesc();
    this.GetUser();
  }

  handleChange = (e) => {
    this.setState({ name: e.target.value });
  }

  onSubmit = e => {
    e.preventDefault();
    const newUser = {
      name: this.state.name,
      email: this.state.email
    };
    this.props.addUser(newUser, this.props.history);

  };

  render() {

    return (
      <>
        <UserHeader />
        <ToastContainer />
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row>
            <Col className="offset-xl-2 offset-xl-2" lg="12" xl="8">
              <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">My account</h3>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Form>
                    <h6 className="heading-small text-muted mb-4">
                      User information
                    </h6>
                    <div className="pl-lg-4">
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-username"
                            >
                              Username
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-username"
                              placeholder="Username"
                              type="text"
                              value={this.state.name}
                              onChange={event => this.setState({ name: event.target.value })}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-email"
                            >
                              Email address
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-email"
                              placeholder="jesse@example.com"
                              type="email"
                              value={this.state.email}
                              onChange={event => this.setState({ email: event.target.value })}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-password"
                            >
                              Password
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-password"
                              placeholder="********"
                              type="password"
                              value={this.state.password}
                              onChange={event => this.setState({ password: event.target.value })}
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-password2"
                            >
                              Password
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-password2"
                              placeholder="********"
                              type="password2"
                              value={this.state.password2}
                              onChange={event => this.setState({ password2: event.target.value })}
                            />
                          </FormGroup>
                        </Col>
                        <Col className="12">
                          <div className="text-center">
                            <Button
                              color="primary"
                              onClick={(e) => this.UpdateProfile()}

                            >
                              Edit
                      </Button>
                          </div>
                        </Col>
                      </Row>

                    </div>

                    <hr className="my-4" />
                    {/* Description */}
                    <h6 className="heading-small text-muted mb-4">Face recognition</h6>
                    <div className="pl-lg-4">
                      <FormGroup>
                        <div className="text-center">
                          <Button
                            disabled={!this.state.faceReady}
                            color="primary"
                            onClick={(e) => this.openCam()}
                          >
                            {this.state.facedata ? "Update face recognition data" : "Save face recognition data"}
                          </Button>
                        </div>
                      </FormGroup>
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
          {this.state.showcam &&
            <div className="modal-cam">
              <div className="video-wrapper">
                <Card className="card-profile shadow">
                  <CardHeader className="bg-white border-0">
                    <Row className="align-items-center">
                      <Col xs="8">
                        <h3 className="mb-0">Face recognition data</h3>
                      </Col>
                      <Col className="text-right" xs="4">
                        <Button
                          color="danger"
                          size="sm"
                          onClick={(e) => this.closeCam()}
                        >
                          <i className=" ni ni-fat-remove" />
                        </Button>
                      </Col>
                    </Row>
                  </CardHeader>
                  <CardBody id="ro">
                    <video id={this.props.id}
                      ref={this.videoTag}
                      autoPlay
                      id="video"
                      onPlay={this.played}>
                    </video>
                    <div className="spinner" hidden={this.state.camReady}>
                      <div className="spinner-box">
                        <div className="circle-border">
                          <div className="circle-core"></div>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </div>
            </div>}
        </Container>
      </>
    );
  }
}

export default Profile;