/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import * as faceapi from 'face-api.js';
import axios from 'axios';

import jwt_decode from "jwt-decode";
import setAuthToken from "../../utils/setAuthToken";
import { setCurrentUser, logoutUser } from "../../actions/authActions";
import store from "../../store";
import Add from "components/Users/Add";
// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Progress,
  Table,
  Container,
  Row,
  Col,
  Button,
  UncontrolledTooltip
} from "reactstrap";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { any } from "@tensorflow/tfjs";
// core components
/* import Header from "components/Headers/Header.js"; */
// Check for token to keep user logged in
if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());

    // Redirect to login
    window.location.href = "./login";
  }
}
class Users extends React.Component {

  constructor() {
    super();
    this.state = {
      activeNav: 1,
      users: [],
      id: 0,
      avatar: 'https://ui-avatars.com/api/?background=3087c3&color=fff&rounded=true&bold=true&&name=',
      showcam: false,
      authorized: false,
      add: false,
      camReady: false
      
    };
    this.videoTag = React.createRef();
    this.played = this.played.bind(this)
    this.closeAd = this.closeAd.bind(this)
  }
  delete = true;

  toggleNavs = (e, index) => {
    e.preventDefault();
    this.setState({
      activeNav: index,
      chartExample1Data:
        this.state.chartExample1Data === "data1" ? "data2" : "data1"
    });
  };
  closeAd() {
    this.setState({
      add: false
    });
    this.GetUsers();
  }
  openAd() {
    this.setState({
      add: true
    })
  }
  componentDidMount() {
    if (localStorage.getItem("face") == "authorized") {
      this.setState({ authorized: true });
    }
    this.GetUsers();
  }
  async openCam() {
    this.setState({ showcam: true });
    Promise.all([
      faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
      faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
      faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
      await faceapi.nets.ssdMobilenetv1.loadFromUri('/models')
    ]).then(this.startVideo())

  }
  closeCam() {
    const tracks = this.videoTag.current.srcObject.getTracks();
    tracks[0].stop();
    this.setState({ showcam: false });
    this.setState({
      camReady: false
    })
  }
  startVideo() {
    navigator.mediaDevices
      .getUserMedia({ video: true })
      .then(stream => this.videoTag.current.srcObject = stream)
      .catch(console.log);
  }
  async played() {

    const vid = document.getElementById('video')
    vid.width = 640
    vid.height = 480
    const ro = document.getElementById('ro')
    const canvas = faceapi.createCanvasFromMedia(vid)

    ro.appendChild(canvas)
    const displaySize = { width: vid.width, height: vid.height }
    faceapi.matchDimensions(canvas, displaySize)
    const labeledFaceDescriptors = await this.loadLabeledDesc()
    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6)
    const entred = false;
    this.setState({
      camReady: true
    })
    let myInterval = setInterval(async () => {
      const detections = await faceapi.detectAllFaces(vid, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceDescriptors()
      if (detections.length > 0) {
        const resizedDetections = faceapi.resizeResults(detections, displaySize)
        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        faceapi.draw.drawDetections(canvas, resizedDetections)
        faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
        const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
        results.forEach((result, i) => {
          const box = resizedDetections[i].detection.box
          const drawBox = new faceapi.draw.DrawBox(box, { label: results.toString(), lineWidth: 5 })
          drawBox.draw(canvas)
          if (entred == false) {
            toast.success('access granted', {
              position: "top-right",
              autoClose: 4000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            setTimeout(async () => {
              localStorage.setItem("face", "authorized")
              this.setState({ authorized: true });
              this.closeCam()
            }, 1000)

            entred = true;
          }
          clearInterval(myInterval);
        })
      }
      /* faceapi.draw.drawDetections(canvas, resizedDetections)
      faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
      faceapi.draw.drawFaceExpressions(canvas, resizedDetections) */

    }, 100)
  }



  loadLabeledImages() {
    const labels = ['jassem', 'wajdi', 'chris_brown', 'donald_trump', 'the_weeknd']
    return Promise.all(
      labels.map(async label => {
        const descriptions = []
        for (let i = 1; i <= 2; i++) {
          const img = await faceapi.fetchImage(`/labeled_images/${label}/${i}.jpg`)
          const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
          descriptions.push(detections.descriptor)
        }
        console.log(new faceapi.LabeledFaceDescriptors(label, descriptions))
        return new faceapi.LabeledFaceDescriptors(label, descriptions)
      })
    )
  }
  async loadLabeledDesc() {
    return new Promise(function (resolve, reject) {
      axios.get('/api/users/descriptors')
        .then(res => {
          const descriptors = res.data;
          return Promise.all(
            descriptors.map(async desc => {
              var array = JSON.parse("[" + desc.descriptors + "]");
              console.log(new faceapi.LabeledFaceDescriptors(desc.label, [new Float32Array(array), new Float32Array(array)]))
              resolve(new faceapi.LabeledFaceDescriptors(desc.label, [new Float32Array(array), new Float32Array(array)]))
            })
          )

        }).catch(err => { console.log(err); reject() });
    });
  }


  GetUsers = () => {
    axios.get('/api/users')
      .then(res => {
        const users = res.data;
        this.setState({ users });
      }).catch(err => { });
  }
  DeleteUser = (id) => {
    axios.delete('/api/users/' + id).then(res => {
      this.GetUsers();
      toast.success('deleted successfully', {
        position: "top-right",
        autoClose: 4000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    })
  }

  
  UpdateProfile = (event,current) => {
    let user;
    if (this.state.password === undefined) {
      user = {
        name: current.name,
        email: current.email,
        role: event.target.value.toString().toLowerCase()

      }
    }
    else {
      user = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        password2: this.state.password2
      }
    }

    console.log(user)
    axios.put('/api/users/' + current._id, user)
      .then(res => {
        this.GetUsers()
        toast.success('Role changed successfully', {
          position: "top-right",
          autoClose: 4000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        
      })
  }
  not = () => {
    alert('called')
  }
  render() {
    return (
      <>
        <ToastContainer />
        <div
          className="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
          style={{
            minHeight: "300px"
          }}
        >
          {/* Mask */}
          <span className="mask bg-gradient-default opacity-8" />
          {/* Header container */}
          <Container className="d-flex align-items-center" fluid>
            <Row>
              <Col lg="7" md="10">
                <h1 className="display-2 text-white">Users management</h1>
                <Button
                  color="info"
                  onClick={(e) => this.openAd()}
                >
                  Add users
                </Button>
              </Col>
            </Row>
          </Container>
        </div>
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <h3 className="mb-0">Users list</h3>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Role</th>
                      <th scope="col">Status</th>
                      <th scope="col">Change role</th>
                      <th scope="col">Operation</th>
                      <th scope="col" />
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.users.map(user =>
                      <tr key={user._id}>
                        <th scope="row">
                          <Media className="align-items-center">
                            <a
                              className="avatar rounded-circle mr-3"
                              href="#pablo"
                              onClick={e => e.preventDefault()}
                            >
                              <img
                                alt="..."
                                src={this.state.avatar + user.name}
                              />
                            </a>
                            <Media>
                              <span className="mb-0 text-sm">
                                {user.name}
                              </span>
                            </Media>
                          </Media>
                        </th>
                        <td>{user.email}</td>
                        <td>
                          {user.role == 'admin' && <Badge color="" className="badge-dot mr-4">
                            <i className="bg-primary" /> Admin
                        </Badge>}
                          {user.role != 'admin' && <Badge color="" className="badge-dot mr-4">
                            <i className="bg-success" /> User
                        </Badge>}
                        </td>
                        <td>
                          <Badge color="success">active</Badge>
                        </td>
                        <td>
                          {user.role == 'admin' &&
                            <select className="form-control" value="admin" onChange={(e) => this.UpdateProfile(e,user)}>
                              <option >Admin</option>
                              <option >User</option>
                            </select>
                          }
                          {user.role != 'admin' &&
                            <select className="form-control" value="user" onChange={(e) => this.UpdateProfile(e, user)}>
                              <option >User</option>
                              <option >Admin</option>
                            </select>
                          }
                        </td>
                        <td className="text-right">
                          {this.state.authorized &&
                            <Button color="warning" onClick={(e) => this.DeleteUser(user._id)}>
                              <i className="fas fa-trash"></i>
                            </Button>}
                          {!this.state.authorized &&
                            <Button color="warning" onClick={(e) => this.openCam()}>
                              <i className="fas fa-trash"></i>
                            </Button>}
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
                <CardFooter className="py-4">
                  <nav aria-label="...">
                    <Pagination
                      className="pagination justify-content-end mb-0"
                      listClassName="justify-content-end mb-0"
                    >
                      <PaginationItem className="disabled">
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                          tabIndex="-1"
                        >
                          <i className="fas fa-angle-left" />
                          <span className="sr-only">Previous</span>
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem className="active">
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          1
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          2 <span className="sr-only">(current)</span>
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          3
                        </PaginationLink>
                      </PaginationItem>
                      <PaginationItem>
                        <PaginationLink
                          href="#pablo"
                          onClick={e => e.preventDefault()}
                        >
                          <i className="fas fa-angle-right" />
                          <span className="sr-only">Next</span>
                        </PaginationLink>
                      </PaginationItem>
                    </Pagination>
                  </nav>
                </CardFooter>
              </Card>
            </div>
          </Row>
          {this.state.add && <div className="modal-cam">
            <div>
              <Add GetUsers={this.not} closeAd={this.closeAd} />
            </div>
          </div>}
          {this.state.showcam &&
            <div className="modal-cam">
              <div className="video-wrapper">
                <Card className="card-profile shadow">
                  <CardHeader className="bg-white border-0">
                    <Row className="align-items-center">
                      <Col xs="8">
                        <h3 className="mb-0">Face recognition</h3>
                      </Col>
                      <Col className="text-right" xs="4">
                        <Button
                          color="danger"
                          size="sm"
                          onClick={(e) => this.closeCam()}
                        >
                          <i className=" ni ni-fat-remove" />
                        </Button>
                      </Col>
                    </Row>
                  </CardHeader>
                  <CardBody id="ro">
                    <video id={this.props.id}
                      ref={this.videoTag}
                      autoPlay
                      id="video"
                      onPlay={this.played}>
                    </video>
                    <div className="spinner" hidden={this.state.camReady}>
                      <div class="spinner-box">
                        <div class="circle-border">
                          <div class="circle-core"></div>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </div>
            </div>}
        </Container>
      </>
    );
  }
}

export default Users;
