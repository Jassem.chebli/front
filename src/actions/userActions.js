import axios from "axios";

import { GET_ERRORS, SET_CURRENT_USER, USER_LOADING } from "./types";

// Register User
export const addUser = (userData, history) => dispatch => {
  axios
    .post("/api/users/add", userData)
    .catch(err =>
      console.log(err)
    );
};