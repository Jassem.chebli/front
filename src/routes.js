/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Index from "views/Index.js";
import Users from "views/examples/Users.js";
import AddUser from "components/Users/Add";
import List from "components/Dictionnary/List.js";
import ScrappingFB from "components/scrapFb/ScrappingFB.js";
import Ad from "components/Scraping/Ad.js";
import Profile from "views/examples/Profile.js";
import Maps from "views/examples/Maps.js";
import Register from "components/Auth/Register.js";
import Login from "components/Auth/Login.js";
import Tables from "views/examples/Tables.js";
import Icons from "views/examples/Icons.js";
import ResultTrain from "components/ML/ResultTrain.js";
import GetLasPots from "components/scrapFb/GetLasPots.js";

var routes = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/admin"
  },
  {
    path: "/predict",
    name: "Predict",
    icon: "ni ni-tv-2 text-primary",
    component: ResultTrain,
    layout: "/admin"
  },
  {
    path: "/ad",
    name: "Ad",
    icon: "ni ni-single-copy-04 text-orange",
    component: Ad,
    layout: "/admin"
  },
  {
    path: "/dictionnary",
    name: "Dictionnary",
    icon: "ni ni-caps-small text-info",
    component: List,
    layout: "/admin"
  },
  {
    path: "/ScrappingFB",
    name: "ScrappingFB",
    icon: "ni ni-caps-small text-info",
    component: ScrappingFB,
    layout: "/admin"
  },
  {
    path: "/GetLasPots",
    name: "GetLasPots",
    icon: "ni ni-caps-small text-info",
    component: GetLasPots,
    layout: "/admin"
  },
  {
    path: "/users",
    name: "Users management",
    icon: "ni ni-circle-08 text-pink",
    component: Users,
    layout: "/admin"
  },
  {
    path: "/user-profile",
    name: "User Profile",
    icon: "ni ni-single-02 text-yellow",
    component: Profile,
    layout: "/admin"
  },
  /* {
    path: "/users/add",
    name: "Add user",
    icon: "ni ni-tv-2 text-primary",
    component: AddUser,
    layout: "/admin"
  },
  {
    path: "/icons",
    name: "Icons",
    icon: "ni ni-planet text-blue",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/maps",
    name: "Maps",
    icon: "ni ni-pin-3 text-orange",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/user-profile",
    name: "User Profile",
    icon: "ni ni-single-02 text-yellow",
    component: Profile,
    layout: "/admin"
  },
  {
    path: "/tables",
    name: "Tables",
    icon: "ni ni-bullet-list-67 text-red",
    component: Tables,
    layout: "/admin"
  },
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: Login,
  },
  {
    path: "/register",
    name: "Register",
    icon: "ni ni-circle-08 text-pink",
    component: Register,
  } */
 
];
export default routes;
